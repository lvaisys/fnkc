
module Test
where

{-
message to validate
board:
+-+-+-+
|O| |X|
+-+-+-+
|X| |X|
+-+-+-+
| | | |
+-+-+-+
-}
message :: String
message = "(l (l  \"x\" 1 \"y\"  0   \"v\" \"x\")  (l   \"x\"   0 \"y\"  0  \"v\"   \"o\") (l  \"x\"  1  \"y\" 2 \"v\"  \"x\") (l  \"x\"  0   \"y\"   2   \"v\"   \"x\"))"