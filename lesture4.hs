module Lecture4 where

import Data.Monoid

message :: String
message = "(l (l  \"x\" 0 \"y\"  0   \"v\" \"o\")  (l   \"x\"   0 \"y\"  0  \"v\"   \"o\") (l  \"x\"  1  \"y\" 2 \"v\"  \"x\") (l  \"x\"  0   \"y\"   2   \"v\"   \"x\"))"

data GesintuvoTipas = A | B | C
  deriving Eq

instance Show GesintuvoTipas where
  show A = "Putos"
  show B = "Milteliai"
  show C = "COdu" 

data Gesintuvas = Gesintuvas {
    talpa :: Int
  , tipas :: GesintuvoTipas
} deriving Show

g1 = Gesintuvas 4 A
g2 = Gesintuvas 8 C
g3 = Gesintuvas 4 C

instance Eq Gesintuvas where
  (Gesintuvas a1 b1) == (Gesintuvas a2 b2) =
    a1 == a2 && b1 == b2

class FuzzyEq a where
  (~=) :: a -> a -> Bool
  (/~=) :: a -> a -> Bool

  a ~= b = not (a /~= b)
  a /~= b = not (a ~= b)

instance FuzzyEq Gesintuvas where
  (Gesintuvas a _) ~= (Gesintuvas b _) = a == b  

instance FuzzyEq Integer where
  a ~= b = abs (a - b) <= 1

instance FuzzyEq GesintuvoTipas

instance Ord Gesintuvas where
  compare (Gesintuvas a _) (Gesintuvas b _) =
    compare a b  

instance Monoid Gesintuvas where
  mempty = Gesintuvas 0 A
  (Gesintuvas a t) `mappend` (Gesintuvas b _) =
    Gesintuvas (a + b) t 